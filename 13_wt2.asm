assume cs:code
code segment
start:
	mov ax,cs
	mov ds,ax
	mov si,offset capital
	mov ax,0
	mov es,ax
	mov di,200h
	mov cx,offset capitalend - offset capital
	cld
	rep movsb
	;安装中断例程
	mov ax,0
	mov es,ax
	mov word ptr es:[7ch*4],200h
	mov word ptr es:[7ch*4+2],0
	;设置中断向量
	mov ax,4c00h
	int 21h

capital:
	push cx
	push si
	;避免寄存器冲突
change:
	mov cl,[si]
	mov ch,0
	jcxz ok
	;用cx提取每一个字符
	and byte ptr [si],11011111b
	inc si
	jmp short change
ok:
	pop si
	pop cx
	iret
capitalend:
	nop
code ends
end start
