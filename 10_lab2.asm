﻿;名称：divdw
;功能：除法，被除数32位，除数16位，商32位，余数16位，不会溢出
;参数：（dx）= 被除数高16位，（ax）= 被除数低16位，（cx）= 除数
;返回：（dx）= 商高16位，（ax）= 商低16位，（cx）= 余数
assume cs:code
code segment
start:
	mov ax,4240H
	mov dx,0FH
	mov cx,0AH
	call divdw
	
	mov ax,4c00h
	int 21h
	
divdw:
	push bx
	push ax
	mov ax,dx
	mov dx,0
	div cx
	mov bx,ax
	pop ax
	div cx
	mov cx,dx
	mov dx,bx
	pop bx
	ret
	
code ends
end start