;用bx向中断例程传送转移位移
assume cs:code,ds:data
data segment
	db 'conversation',0
data ends
code segment
start:
	mov ax,cs
	mov ds,ax
	mov si,offset jp

	mov ax,0
	mov es,ax
	mov di,200h

	mov cx,offset jpend - offset jp
	cld
	rep movsb
	;安装中断例程
	mov word ptr es:[7ch*4],200h
	mov word ptr es:[7ch*4+2],0
	;设置中断向量
	mov ax,data
	mov ds,ax
	mov si,0
	mov ax,0b800h
	mov es,ax
	mov di,12*160
s:	
	cmp byte ptr [si],0
	je ok
	;如果是0跳出循环
	mov al,[si]
	mov es:[di],al
	;显示在屏幕上
	inc si
	add di,2
	mov bx,offset s - offset ok
	int 7ch
	;测试int 7ch
ok:
	nop
	mov ax,4c00h
	int 21h

jp:
	push bp
	mov bp,sp
	add [bp+2],bx
	pop bp
	iret
	;中断例程
jpend:
	nop
code ends
end start
