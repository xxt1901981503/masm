assume cs:codesg,ds:datasg,ss:stacksg
datasg segment
	dw 0123H,0456H,0789H,0abcH,0defH,0fedH,0cbaH,0987H
datasg ends

stacksg segment
	dw 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
stacksg ends

codesg segment
start:	mov ax,stacksg
	mov ss,ax
	mov sp,20H
	
	mov ax,datasg
	mov ds,ax

	mov bx,0

	mov cx,8
s:	push [bx]
	add bx,2
	loop s

	mov bx,0

	mov cx,8
s0:	pop [bx]
	add bx,2
	loop s0

	mov ax,4c00H
	int 21H
codesg ends
end start
