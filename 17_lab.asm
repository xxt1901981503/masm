;用ah寄存器传递功能号，0表示读，1表示写
;用dx寄存器传递要读写的扇区的逻辑扇区号
;用es:bx指向存储读出数据或写入数据的内存区
;
;逻辑扇区号＝(面号 * 80＋磁道号) * 80＋扇区号 －1
;int()：描述性运算符，取商
;rem()：描述性运算符，取余数
;面号 ＝ int（逻辑扇区号 / 1440）
;磁道号＝int（rem（逻辑扇区号 / 1440）/ 18）
;扇区号＝rem（rem（逻辑号扇区号 / 1440） / 18）＋1
assume cs:code,ds:data
data segment
	db 'wo shi xxt!'
data ends
code segment
start:
	mov ax,0
	mov es,ax
	mov word ptr es:[7dh*4],offset do0	
	mov word ptr es:[7dh*4+2],cs
	;没有安装，程序运行完毕后，中断例程的寿命也就结束了
	mov bx,0
	mov ax,data
	mov es,ax
	mov dx,0		;从逻辑扇区号0开始写
	mov ah,1		;1表示写数据
	int 7dh
	
	mov ax,4c00h
	int 21h
do0:
	;ah表示读写，dx表示逻辑扇区号，es:bx写入或读出的缓存区
	jmp short set		
	table dw re,wr
set:
 	push bx
	cmp ah,1
	ja ok
	mov bl,ah
	mov bh,0
	add bx,bx
	call word ptr table[bx]
ok:
	pop bx
	iret
re:
	mov ax,dx
	mov dx,0
	mov cx,1440
	div cx			;ax是商,dx是余数
	mov bx,dx		;余数给bx还要继续除法
	mov dh,al		;面号
	mov cl,18
	mov ax,bx		;余数继续除法
	div cl
	mov ch,al		;磁道号
	add ah,1
	mov cl,ah		;扇区号
	mov dl,0		;驱动器号
	mov al,1

	mov ah,2		;实际参数2表示读
	int 13h
	ret
wr:
	mov ax,dx
	mov dx,0	
	mov cx,1440
	div cx
	mov bx,dx		;余数给bx还要继续除法
	mov dh,al		;面号
	mov cl,18
	mov ax,bx		;余数继续除法
	div cl
	mov ch,al		;磁道号
	add ah,1
	mov cl,ah
	mov dl,0		;驱动器号
	mov al,1

	mov ah,3		;实际参数3表示写
	int 13h
	ret
do0end:
	nop
code ends
end start
