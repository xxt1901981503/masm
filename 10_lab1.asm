;名称：show_str
;功能：在屏幕的指定位置，用指定颜色，显示一个用0结尾的字符串
;参数：（dh）= 行号，（dl）= 列号（取值范围0~80），（cl）= 颜色，ds:si：该字符串的首地址
;返回：显示在屏幕上
assume cs:code,ds:data
data segment
	db 'fghfgh',0
data ends
code segment
start:
	mov dh,4
	mov dl,4
	mov cl,2
	mov ax,data
	mov ds,ax
	mov si,0
	call show_str
	
	mov ax,4c00h
	int 21h
	
show_str:
	push ax
	push cx
	push dx
	push es
	push si
	push di
	mov ax,0b800h
	mov es,ax
	dec dh
	mov al,160
	mul dh
	add dl,dl
	mov dh,0 		;计算显示在屏幕位置
	add ax,dx
	mov di,ax 
	mov ah,cl 
	x:
	mov cl,ds:[si]
	mov ch,0 
	jcxz f 
	mov al,cl
	mov es:[di],ax 
	inc si 
	inc di 
	inc di 
	jmp x
	f:
	pop di 
	pop si
	pop es
	pop dx 
	pop cx 
	pop ax
	ret
code ends
end start