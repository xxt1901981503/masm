;ah传递功能号：0清屏，1设置前景色，2设置背景色,向上滚动一行
;用al传送颜色值
assume cs:code
code segment
start:
	mov ax,0
	mov ds,ax
	mov word ptr ds:[7ch*4],200h
	mov word ptr ds:[7ch*4+2],0
	;设置中断向量表
	mov ax,cs
	mov ds,ax
	mov si,offset int7ch
	mov ax,0
	mov es,ax
	mov di,200h	
	mov cx,offset int7chend - offset int7ch
	cld
	rep movsb
	;安装中断例程
	mov ah,1
	mov al,2
	int 7ch
	;测试程序
	mov ax,4c00h
	int 21h

	ORG 200h
	;伪指令，表示下一条指令从偏移地址200h开始，正好和安装后的偏移地址同
	;因为如果没有ORG 200h，此中断例程被安装后，标号所代表的地址变了，和之前编译器有别
int7ch:
	jmp short begin
	table dw sub1,sub2,sub3,sub4
begin:
	push bx
	cmp ah,3
	ja f
	mov bl,ah
	mov bh,0
	add bx,bx
	call word ptr table[bx]
f:
	pop bx
	iret

sub1:
	push bx
	push cx
	push es
	mov bx,0b800h
	mov es,bx
	mov bx,0
	mov cx,2000
sub1s:
	mov byte ptr es:[bx],' '
	add bx,2
	loop sub1s
	pop es
	pop cx
	pop bx
	ret

sub2:
	push bx
	push cx
	push es
	
	mov bx,0b800h
	mov es,bx
	mov bx,1
	mov cx,2000
sub2s:
	and byte ptr es:[bx],11111000b
	or es:[bx],al
	add bx,2
	loop sub2s
	
	pop es
	pop cx
	pop bx
	ret

sub3:
	push bx
	push cx
	push es
	mov cl,4
	shl al,cl
	mov bx,0b800h
	mov es,bx
	mov bx,1
	mov cx,2000
sub3s:
	and byte ptr es:[bx],10001111b
	or es:[bx],al
	add bx,2
	loop sub3s
	pop es
	pop cx
	pop bx
	ret

sub4:
	push cx
	push si
	push di
	push es
	push ds

	mov si,0b800h
	mov es,si
	mov ds,si
	mov si,160
	mov di,0
	cld
	mov cx,21
sub4s:
	push cx
	mov cx,160
	rep movsb
	pop cx
	loop sub4s
	
	mov cx,80
	mov si,0
sub4s1:
	mov byte ptr [160*24+si],' '
	add si,2
	loop sub4s1

	pop ds
	pop es
	pop di
	pop si
	pop cx
	ret
int7chend:
	nop
code ends
end start
