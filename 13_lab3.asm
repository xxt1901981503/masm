assume cs:code
code segment
	s1: db 'Good better,best,','$'
	s2: db 'never let it rest,','$'
	s3: db 'Till good is better,','$'
	s4: db 'And better,best.','$'
	s: dw offset s1,offset s2,offset s3,offset s4
	row: db 2,4,6,8
start:
	mov ax,cs
	mov ds,ax
	mov bx,offset s
	mov si,offset row
	mov cx,4
ok:
	mov bh,0	;页号
	mov dh,[si]	;行号
	mov dl,0;	;列号
	mov ah,2	;子程序的编号
	int 10h
	;置光标
	mov dx,[bx]
	mov ah,9
	int 21h
	;在光标位置显示字符串
	inc si
	add bx,2
	loop ok

	mov ax,4c00h
	int 21h
code ends
end start
