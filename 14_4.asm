assume cs:code
code segment
start:
	mov al,8
	out 70h,al
	in al,71h
	
	mov ah,al	;al中为从 CMOS RAM 的8号单元中读出的数据
	mov cl,4
	shr ah,cl	;ah为月份的十位数码值
	and al,00001111b	;al为个位数码值

	add ah,30h
	add al,30h
	;BCD 码值 + 30h = 十进制对应的 ASCII 码
	mov bx,0b800h
	mov es,bx
	mov byte ptr es:[160*12+40*2],ah
	mov byte ptr es:[160*12+40*2+2],al
	
	mov ax,4c00h
	int 21h
code ends
end start
