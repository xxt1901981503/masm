;任务：将实验7中的Power idea公司的数据按照图10.所示的格式在屏幕上显示出来
assume cs:code,ds:data
data segment
	db '1975','1976','1977','1978','1979','1980','1981','1982','1983'
	db '1984','1985','1986','1987','1988','1989','1990','1991','1992'
	db '1993','1994','1995'
	;以上是表示21年的21个字符串
	dd 16,22,382,1356,2390,8000,16000,24486,50065,97479,140417,197514
	dd 345980,590827,803530,118300,1843000,2759000,3753000,4649000,5937000
	;以上是表示21年公司总收入的21个dword型数据
	dw 3,7,9,13,28,38,130,220,476,778,1001,1442,2258,2793,4037,5635,8226
	dw 11542,14430,15257,17800
	;以上是表示21年公司雇员人数的21个word型数据
data ends
agency segment
	db 8 dup(0)
agency ends
code segment
start:
	mov ax,0b800H
	mov es,ax
	mov di,0 
	mov cx,80*24 
x:
	mov byte ptr es:[di],' '		;将屏幕清空
	mov byte ptr es:[di+1],0 
	inc di
	inc di
	loop x
	mov ax,data
	mov es,ax
	mov di,0
	mov bx,0 
	mov ax,agency
	mov ds,ax
	mov si,0
	mov dh,4
	mov cx,21
x1:
	push cx
	mov ax,es:[di]
	mov ds:[si],ax
	mov ax,es:[di+2]
	mov ds:[si+2],ax
	mov byte ptr ds:[si+4],0 		;显示年份
	mov dl,0
	mov cl,2 
	call show_str
	mov ax,es:[84+di]
	push dx
	mov dx,es:[84+di+2]
	call dtoc_dword		;显示收入
	pop dx
	mov dl,20
	mov cl,2
	call show_str
	mov ax,es:[84+84+bx]
	call dtoc_word
	mov dl,40 
	mov cl,2 
	call show_str
	mov ax,es:[84+di]
	push dx
	mov dx,es:[84+di+2]
	div word ptr es:[84+84+bx]		;计算人均收入并显示
	call dtoc_word
	pop dx
	mov dl,60
	mov cl,2 
	call show_str
	add di,4
	add bx,2 
	add dh,1 
	pop cx 
	loop x1
	mov ah,0 
	int 16h		;加上按任意键继续功能，可以直接双击运行
	
	mov ax,4c00h
	int 21h
	
show_str:
	push ax
	push cx
	push dx
	push es
	push si
	push di
	mov ax,0b800h
	mov es,ax
	dec dh
	mov al,160
	mul dh
	add dl,dl
	mov dh,0 		;计算显示在屏幕位置
	add ax,dx
	mov di,ax 
	mov ah,cl 
	x_show:
	mov cl,ds:[si]
	mov ch,0 
	jcxz f_show 
	mov al,cl
	mov es:[di],ax 
	inc si 
	inc di 
	inc di 
	jmp x_show
	f_show:
	pop di 
	pop si
	pop es
	pop dx 
	pop cx 
	pop ax
	ret
	
dtoc_word:
	push ax
	push bx
	push cx
	push dx
	push si
	mov bx,0
	
	x_dtoc_word:
	mov dx,0
	mov cx,10
	div cx
	mov cx,ax
	add dx,'0'
	push dx
	inc bx
	jcxz f_dtoc_word
	jmp x_dtoc_word

	f_dtoc_word:
	mov cx,bx
	x1_dtoc_word:
	pop ds:[si]
	inc si
	loop x1_dtoc_word 
	pop si
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	
dtoc_dword:
	push ax
	push bx
	push cx
	push dx
	push si
	mov bx,0
	
	x_dtoc_dword:
	mov cx,10
	call divdw
	push cx
	inc bx
	cmp ax,0 
	jne x_dtoc_dword
	cmp dx,0 
	jne x_dtoc_dword
	mov cx,bx
	x1_dtoc_dword:
	pop ds:[si]
	add byte ptr ds:[si],'0'
	inc si
	loop x1_dtoc_dword
	pop si
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	
divdw:
	push bx
	push ax
	mov ax,dx
	mov dx,0
	div cx
	mov bx,ax
	pop ax
	div cx
	mov cx,dx
	mov dx,bx
	pop bx
	ret
	
code ends
end start