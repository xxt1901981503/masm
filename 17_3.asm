;charstack：字符栈的入栈，出栈，显示。用ah表示功能号
;对于0号功能：(al)=入栈字符，对于1号功能，(al)=返回的字符
;对于2号功能：(dh),(dl)=字符串在屏幕上显示的行、列位置
;ds:si指向字符串的存储空间，字符串以0为结尾符

assume cs:code,ds:data

data segment
	db 128 dup (0)
data ends

code segment
start:
	mov ax,data
	mov ds,ax
	mov si,0
	call getstr
	mov ax,4c00h
	int 21h

charstack:
	jmp short charstart
	table dw charpush,charpop,charshow
	top dw  0
charstart:
	push bx
	push dx
	push di
	push es

	cmp ah,2
	ja sret
	mov bl,ah
	mov bh,0
	add bx,bx
	jmp word ptr table[bx]
charpush:
	mov bx,top
	mov [si][bx],al
	inc top
	jmp sret
charpop:
	cmp top,0
	je sret
	dec top
	mov bx,top
	mov al,[si][bx]
	jmp sret
charshow:
	mov bx,0b800h
	mov es,bx
	mov al,160
	mov ah,0		;dx
	mul dh			;160*行
	mov di,ax
	add dl,dl		;2*列
	mov dh,0
	add di,dx
	
	mov bx,0
charshows:
	cmp bx,top
	jne noempty
	mov byte ptr es:[di],' '
	jmp sret
noempty:
	mov al,[si][bx]
	mov es:[di],al
	mov byte ptr es:[di+2],' '
	inc bx
	add di,2
	jmp charshows			;清除屏幕上上一次显示的内容
sret:
	pop es
	pop di
	pop dx
	pop bx
	ret

getstr:
	push ax
getstrs:
	mov ah,0
	int 16h
	cmp al,20h		;ASCII 小于20h，说明不是字符
	jb nochar
	mov ah,0
	call charstack		;字符入栈
	mov ah,2
	call charstack		;显示栈中的字符
	jmp getstrs
nochar:
	cmp ah,0eh		;退格键的扫描码
	je backspace
	cmp ah,1ch		;Enter 键的扫描码
	je enter
	jmp getstrs
backspace:
	mov ah,1
	call charstack		;字符出栈
	mov ah,2
	call charstack		;显示栈中的字符
	jmp getstrs
enter:
	mov al,0
	mov ah,0
	call charstack		;0 入栈
	mov ah,2
	call charstack
	pop ax
	ret
code ends
end start
