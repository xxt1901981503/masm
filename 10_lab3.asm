;名称：dtoc-word
;功能：将一个word型数转化为字符串
;参数：（ax）= word型的数据，ds:si指向字符串的首地址
;返回：ds:[si]放此字符串，以0结尾
assume cs:code,ds:data
data segment
	db 20 dup(1)
data ends
code segment
start:
	mov ax,data
	mov ds,ax
	mov ax,12666
	mov si,0
	call dtoc
	mov dh,8
	mov dl,3
	mov cl,2
	call show_str
	
	mov ax,4c00H
	int 21h
	
dtoc:
	push ax
	push bx
	push cx
	push dx
	push si
	mov bx,0
	
	x:
	mov dx,0
	mov cx,10
	div cx
	mov cx,ax
	add dx,'0'
	push dx
	inc bx
	jcxz f
	jmp x

	f:
	mov cx,bx
	x1:
	pop ds:[si]
	inc si
	loop x1 
	pop si
	pop dx
	pop cx
	pop bx
	pop ax
	ret
	
show_str:
	push ax
	push cx
	push dx
	push es
	push si
	push di
	mov ax,0b800h
	mov es,ax
	dec dh
	mov al,160
	mul dh
	add dl,dl
	mov dh,0 		;计算显示在屏幕位置
	add ax,dx
	mov di,ax 
	mov ah,cl 
	_x:
	mov cl,ds:[si]
	mov ch,0 
	jcxz _f 
	mov al,cl
	mov es:[di],ax 
	inc si 
	inc di 
	inc di 
	jmp _x
	_f:
	pop di 
	pop si
	pop es
	pop dx 
	pop cx 
	pop ax
	ret
code ends
end start