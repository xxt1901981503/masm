	assume cs:code,ds:date,ss:stack
	date segment
	db 0;direction [0]                            23H是食物,20H是空格,2Ah是身体
	dw 0;head.....  high  列 low 行[1]
	dw 0;tail [3]
	dw 256 dup (0);临时保存拐点[5](其实拐点我本来想用栈保存的,但是发现拐点的处理需要用到中断,用栈的话 会造成混乱)
	Times dw 0,0
	date ends
;/////////////////////////////////////////////////////
	stack segment
	db 256 dup (0)
	stack ends
;///////////////////////////////////////////////////
	code segment
  start:mov ax,date
	mov ds,ax
	mov ax,stack
	mov ss,ax
	mov sp,256
	mov byte ptr ds:[0],4; 初始向右
	mov byte ptr ds:[1],12;头 ，行
	mov byte ptr ds:[2],40;头 ，列
 	mov byte ptr ds:[3],12;尾 ，行
	mov byte ptr ds:[4],38;尾 ，列
	call clearscreen;清空显存，显示三点
	call food;产生一个食物
	call install;安装 int9 程序
  main: cli;禁止中断
	mov al,ds:[0]
	mov ah,0
	cmp ax,1
	je up;跳转到上
	cmp ax,2
	je down;跳转到下
 	cmp ax,3
	je left;跳转到左
	cmp ax,4
	je right;跳转到右
     up:mov ax,ds:[1];high 列 low 行
	sub al,1;向上 行-1
	cmp al,11111111b; 超界
	je over;结束
	mov ds:[1],ax;
	jmp ok1;
   down:mov ax,ds:[1]
	add al,1;向下 行+1
	cmp al,25;超界
	je over
	mov ds:[1],ax
	jmp ok1
   left:mov ax,ds:[1]
	sub ah,1;列 -1
	cmp ah,11111111b
	je over
	mov ds:[1],ax
	jmp ok1
  right:mov ax,ds:[1]
	add ah,1;
	cmp ah,80
	je over
	mov ds:[1],ax
   ok1: sti ;允许中断
	mov ax,0B800h
	mov es,ax;es,为显存段地址
	mov dx,ds:[1]
	call show;将偏移地址保存在bp中
	mov al,es:[bp]
	cmp al,20h;判断是否有显示
	jne longer;若有显示
	mov byte ptr es:[bp],2Ah;显示一个*
	call cleantail;清除尾部，变化
moveends:call time;延时
	jmp main
;/////////////////////////////////////////////
 longer:cmp al,23h;判断是否是食物
	jne over;不是,输
	mov byte ptr es:[bp],2Ah;
	call food;产生新食物
	jmp moveends;
  over: call time
	call time
	call time
	mov ax,4C00H
	int 21H
;//////////////////////////////////////////结束了,下面是子程序
show:	push dx;dl为行dh列
	push ax
	mov ax,0
	mov bp,0;清空
	mov al,160;
	mul dl ;行*160
	add bp,ax
	mov al,2
	mov ah,0
	mul dh;列乘2
	add bp,ax
	pop ax
	pop dx
	ret
;////////////////////////////////////////////
clearscreen:push es
	push ax
	push dx
	push cx
	push bp
	mov cx,2000h 
	mov ah,01h 
	int 10h
	mov es,ax
	mov dl,0;行为0
	mov cx,25;25次循环
     as:mov dh,0;列为0
	push cx
	mov cx,80;80次循环
    as1:call show;偏移地址在bp中
	mov byte ptr es:[bp],20h
	add dh,1;列加1
	loop as1
	pop cx
	add dl,1;行+1
	loop as;/////////////清空结束
	mov dl,12
	mov dh,38
	call show
	mov byte ptr es:[bp],2ah
	mov dh,39
	call show
	mov byte ptr es:[bp],2ah
	mov dh,40
	call show
	mov byte ptr es:[bp],2ah
	pop bp
  	pop cx
	pop dx
	pop ax
 	pop es
	ret;返回
;////////////////////////////////////////////////////////
time:  	push ax	
	push dx
	mov dx,1500h
	mov ax,0
  times1:sub ax,1
	sbb dx,0
	cmp ax,0
	jne times1
	cmp dx,0
	jne times1
	pop dx
	pop ax
	ret;返回
;////////////////////////////////////////////////
food: 	push bx
	push es
	push bp
	push dx
	push ax
	sti;允许中断
	mov ax,0B800h
	mov es,ax
foodloop:mov ah,0;读时钟计数器
	int 1ah
	mov ax,dx
	and ah,3
	mov dl,80
	div dl;除以80 ,产生0`79
	mov bh,ah
	mov al,0
	mov ah,0
	int 1ah
	mov ax,dx
	and ah,3
	mov dl,25
	div dl;除以25
	mov bl,ah
	mov dx,bx
	call show
	mov dl,byte ptr es:[bp]
	cmp dl,20h
	jne foodloop;如果有显示,循环
	mov byte ptr es:[bp],23h
	pop ax
	pop dx
	pop bp
	pop es
	pop bx
	ret
;//////////////////////////////////////////////
install:push si
	push ds
	push es
	push di
	mov ax,0
	mov es,ax
	mov si,offset int9
	push cs
	pop ds;           设置ds:si 指向源地址
	mov di,204h;         设置es:di 指向目标地址
	mov cx,offset int9end-offset int9;                  cx 传输长度
	cld;正向
	rep movsb
	push es:[9*4]
	pop es:[200h]
	push es:[9*4+2]
	pop es:[202h];将原int9 地址保存在200h
asdf:	cli;禁止中断
	mov word ptr es:[9*4],204h
	mov word ptr es:[9*4+2],0
	pop di
	pop es
	pop ds
	pop si
 	ret;返回////////////////////////////////////以下是新int9
int9:   push ax
	push bx
	in al,60h;读取	
	pushf
	call dword ptr cs:[200h];执行原int9
	cmp al,48h;是否为上
 	je changeup
	cmp al,50h;是否为下
	je changedown
	cmp al,4Bh;左
	je changeleft
	cmp al,4dh;右
	je changeright
	jmp endover
changeup:mov byte ptr ds:[0],1
	jmp changeover
changedown:mov byte ptr ds:[0],2
	jmp changeover
changeleft:mov byte ptr ds:[0],3
	jmp changeover
changeright:mov byte ptr ds:[0],4
	jmp changeover
changeover:mov bx,3
loopchange:add bx,2;拐点起始地址为5,ds为date段
	mov ax,ds:[bx]
	cmp ax,0
	jne loopchange
	mov ax,ds:[1]
	mov ds:[bx],ax    ; mov word ptr ds:[bx],ds:[1]将头地址保存为拐角地址
endover:pop bx
	pop ax
	iret;返回
int9end:nop
;////////////////////////////////////////////////////////////////////////////以下的尾部处理程序(该算法的精华)
cleantail:push ax
	push dx
	push bp
	push es
	push bx
	mov ax,0B800h;
	mov es,ax
cleanloop:mov ax,ds:[5];即第一个拐点
	cmp ax,0;判断是否有拐点
	jne changetail;若有
	mov ax,ds:[1]
changetail:mov dx,ds:[3]   ;dx储存蛇尾,ax储存拐点(蛇头)
	cmp dl,al          ;判断是否同一行
	ja ahang	;蛇尾行更大, 则蛇尾在下面
	jb bhang	;蛇尾行更小,则蛇尾在上面
	cmp dh,ah      ;判断是否同一列
	ja alie       ;蛇尾列大于,则尾部在拐点右边
	jb blie    ;蛇尾列小于,则尾部在拐点左边///////////////////////若都等于,那么此时蛇尾就是拐点,要清除第一个拐点
	mov bp,3
 gdloop:add bp,2
	mov ax,ds:[bp]
	cmp ax,0
	jne gdloop
	sub bp,2;此时bp保存的为最后一个拐点地址
        mov bx,3
gdloop2:add bx,2
	mov ax,ds:[bx+2]           ;mov word ptr ds:[bx],ds:[bx+2]
	mov ds:[bx],ax
	cmp bx,bp
	jne gdloop2;全部前移,将第一个拐点覆盖
	jmp cleanloop;重新判断蛇尾
ahang:  call show
	mov byte ptr es:[bp],20h;清除蛇尾
	sub dl,1;蛇尾向上一行
	mov ds:[3],dx;更新蛇尾
	jmp cends
bhang:  call show
	mov byte ptr es:[bp],20h;清除蛇尾
	add dl,1;蛇尾向下一行
	mov ds:[3],dx;更新蛇尾
	jmp cends
alie:   call show
	mov byte ptr es:[bp],20h;清除蛇尾
	sub dh,1;蛇尾向左一列
	mov ds:[3],dx;更新蛇尾
	jmp cends
blie:   call show
	mov byte ptr es:[bp],20h;清除蛇尾
	add dh,1;蛇尾向右一列
	mov ds:[3],dx;更新蛇尾
	jmp cends
cends:	pop bx
	pop es
	pop bp
 	pop dx
 	pop ax
	ret;返回
;///////////////////////////至此,整个程序结束
code ends
end start
