assume cs:code,ds:data
data segment
	db 'welcome to masm!',0
data ends
code segment
start:
	mov ax,cs
	mov ds,ax
	mov si,offset dp
	mov ax,0
	mov es,ax
	mov di,200h
	mov cx,offset dpend - offset dp
	cld
	rep movsb
	;安装中断例程
	mov word ptr es:[7ch*4],200h
	mov word ptr es:[7ch*4+2],0

	mov dh,10	;行号
	mov dl,10	;列号
	mov cl,2	;颜色
	mov ax,data
	mov ds,ax
	mov si,0
	;ds:si指向字符串首地址
	int 7ch
	mov ax,4c00h
	int 21h
dp:
	mov al,160
	mul dh
	add dl,dl
	mov dh,0
	add ax,dx
	mov di,ax
	mov ax,0b800h
	mov es,ax
s:
	mov al,ds:[si]
	mov ah,0
	cmp ax,0
	je f
	mov ah,cl
	mov es:[di],ax
	inc si
	inc di
	inc di
	jmp s
f:
	iret
dpend:
	nop
code ends
end start
