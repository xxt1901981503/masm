assume cs:code,ds:data
data segment
	db "Beginner's All-purpose Symbolic Instruction Code.",0
data ends
code segment
start:
	mov ax,data
	mov ds,ax
	mov si,0
	call letterc
	
	mov ax,4c00h
	int 21h
letterc:
	push si
	push ax
	x: mov al,ds:[si]
	cmp al,0
	je f
	inc si
	cmp al,'a'
	jb x
	cmp al,'z'
	ja x
	add al,'A'-'a'
	mov ds:[si-1],al
	jmp x
	f: pop ax
	pop si
	ret
	
code ends
end start