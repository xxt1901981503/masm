;将当前屏幕的内容保存在磁盘上
;返回成功：(ah)=0,(al)=写入的扇区数
;返回失败：(ah)=出错代码
assume cs:code
code segment
start:
	mov ax,0b800h
	mov es,ax
	mov bx,0	;es:bx指向将写入扇区的数据

	mov al,8	;写入的扇区数
	mov ch,0	;磁道号
	mov cl,1	;扇区号
	mov dl,0	;驱动器号
	mov dh,0	;磁头号

	mov ah,3	;3表示写扇区
	int 13h
	
	mov ax,4c00h
	int 21h
code ends
end start
